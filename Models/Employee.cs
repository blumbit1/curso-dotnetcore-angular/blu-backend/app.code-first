﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace App.CodeFirst.Models
{
    public class Employee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid EmployeeId { get; set; }

        // En la BBDD se va a transformar en un nvarchar(MAX)
        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$")]
        public string? FirstName { get; set; }

        [Required]
        [StringLength(85)]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$")]
        public string? LastName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [StringLength(15)]
        public string? PhoneNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string? Email { get; set; }

        [Required]
        [StringLength(6)]
        public string? Gender { get; set;}

        // Iniciando la variable booleana con el valor true/false
        public Boolean IsDeleted { get; set; } = false;
    }
}
