﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace App.CodeFirst.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(85)", maxLength: 85, nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(6)", maxLength: 6, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeId", "DateOfBirth", "Email", "FirstName", "Gender", "IsDeleted", "LastName", "PhoneNumber" },
                values: new object[,]
                {
                    { new Guid("209d5f79-b7bf-4136-9aca-13bc4a44d3dd"), new DateTime(1985, 11, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "may.kosovov@gmail.com", "May", "Male", false, "Kosovov", "111-222-3333" },
                    { new Guid("8b695255-f7e2-4c51-b8f6-59b6de08f2dd"), new DateTime(1981, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "jan.kirsten@gmail.com", "Jan", "Female", false, "Kirsten", "111-222-3333" },
                    { new Guid("9e6ef25e-f6c0-4498-82af-96ed7edf22eb"), new DateTime(1989, 7, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "carlton.banks@gmail.com", "Carlton", "Male", false, "Banks", "111-222-3333" },
                    { new Guid("c08fd9d1-e415-45ec-8722-0c17bf7fc320"), new DateTime(1979, 4, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "uncle.bob@gmail.com", "Uncle", "Male", false, "Bob", "999-888-7777" },
                    { new Guid("fc85d47b-188f-45b1-b215-4d249ef9ead2"), new DateTime(1975, 9, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "neil.sedakas@gmail.com", "Neil", "Male", false, "Sedakas", "111-222-3333" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");
        }
    }
}
