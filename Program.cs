using App.CodeFirst.DBContext;
using App.CodeFirst.Models;
using App.CodeFirst.Repository;
using App.CodeFirst.ServiceImpl;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Logging
builder.Logging.ClearProviders();
builder.Logging.AddConsole();

// Add DBContext configuration.
builder.Services.AddDbContext<EmployeeDBContext>(opts => opts.UseSqlServer(builder.Configuration.GetConnectionString("EmployeeConnectionString")));

// Scope for Repository and Service Employee
builder.Services.AddScoped<IEmployeeRepository<Employee>, EmployeeServiceImpl>();

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
