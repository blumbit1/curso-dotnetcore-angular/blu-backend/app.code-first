﻿using App.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace App.CodeFirst.DBContext
{
    public class EmployeeDBContext : DbContext
    {
        // Constructor
        public EmployeeDBContext(DbContextOptions options): base(options) { }

        // Creacion de la Tabla en la BBDD
        public DbSet<Employee> Employees {  get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().
                HasData(
                new Employee
                {
                    EmployeeId = Guid.NewGuid(),
                    FirstName = "Uncle",
                    LastName = "Bob",
                    Email = "uncle.bob@gmail.com",
                    DateOfBirth = new DateTime(1979, 04, 25),
                    PhoneNumber = "999-888-7777",
                    Gender = "Male",
                    IsDeleted = false 
                }, 
                new Employee
                {
                    EmployeeId = Guid.NewGuid(),
                    FirstName = "Jan",
                    LastName = "Kirsten",
                    Email = "jan.kirsten@gmail.com",
                    DateOfBirth = new DateTime(1981, 07, 13),
                    PhoneNumber = "111-222-3333",
                    Gender = "Female",
                    IsDeleted = false

                },
                new Employee
                {
                    EmployeeId = Guid.NewGuid(),
                    FirstName = "May",
                    LastName = "Kosovov",
                    Email = "may.kosovov@gmail.com",
                    DateOfBirth = new DateTime(1985, 11, 13),
                    PhoneNumber = "111-222-3333",
                    Gender = "Male",
                    IsDeleted = false
                },
                new Employee
                {
                    EmployeeId = Guid.NewGuid(),
                    FirstName = "Neil",
                    LastName = "Sedakas",
                    Email = "neil.sedakas@gmail.com",
                    DateOfBirth = new DateTime(1975, 09, 3),
                    PhoneNumber = "111-222-3333",
                    Gender = "Male",
                    IsDeleted = false
                },
                new Employee
                {
                    EmployeeId = Guid.NewGuid(),
                    FirstName = "Carlton",
                    LastName = "Banks",
                    Email = "carlton.banks@gmail.com",
                    DateOfBirth = new DateTime(1989, 07, 12),
                    PhoneNumber = "111-222-3333",
                    Gender = "Male",
                    IsDeleted = false
                }
                );
        }
    }
}
