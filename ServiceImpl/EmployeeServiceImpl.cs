﻿using App.CodeFirst.DBContext;
using App.CodeFirst.Models;
using App.CodeFirst.Repository;
using Microsoft.EntityFrameworkCore;

namespace App.CodeFirst.ServiceImpl
{
    public class EmployeeServiceImpl : IEmployeeRepository<Employee>
    {
        readonly EmployeeDBContext _employeeContext;

        // Constructor
        public EmployeeServiceImpl(EmployeeDBContext dbContext) 
        {
            _employeeContext = dbContext;
        }

        public async Task<IEnumerable<Employee>> GetAll()
        {
            return await _employeeContext.Employees.ToListAsync();
        }

        #pragma warning disable CS8613 // Nullability of reference types in return type doesn't match implicitly implemented member.
        public async Task<Employee?> FindById(Guid id)
        #pragma warning restore CS8613 // Nullability of reference types in return type doesn't match implicitly implemented member.
        {
            return await _employeeContext.Employees.FirstOrDefaultAsync(e => e.EmployeeId == id);
        }

        public async void Add(Employee? entity)
        {
            #pragma warning disable CS8604 // Possible null reference argument.
            _ = await _employeeContext.Employees.AddAsync(entity);
            #pragma warning restore CS8604 // Possible null reference argument.
            _ = _employeeContext.SaveChangesAsync();
        }

        public async void Update(Employee employee, Employee employeeUpdated)
        {
                employee.FirstName = employeeUpdated.FirstName;
                employee.LastName = employeeUpdated.LastName;
                employee.Email = employeeUpdated.Email;
                employee.DateOfBirth = employeeUpdated.DateOfBirth;
                employee.PhoneNumber = employeeUpdated.PhoneNumber;
                employee.Gender = employeeUpdated.Gender;
                employee.IsDeleted = employeeUpdated.IsDeleted;

                _ = _employeeContext.SaveChangesAsync();          
        }

        public void Delete(Employee employee)
        {
            _employeeContext.Employees.Remove(employee);
            _employeeContext.SaveChanges();
        }
    }
}
