﻿namespace App.CodeFirst.Repository
{
    public interface IEmployeeRepository<TEntity>
    {
        // Employee CRUD Methods definition with Task, Async and Await
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity>  FindById(Guid id);
        void Add(TEntity entity);
        void Update(TEntity dbEntity, TEntity entity);
        void Delete(TEntity entity);
    }
}
