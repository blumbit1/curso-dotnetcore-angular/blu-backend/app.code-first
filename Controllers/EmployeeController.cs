﻿using App.CodeFirst.Models;
using App.CodeFirst.Repository;
using Microsoft.AspNetCore.Mvc;

namespace App.CodeFirst.Controllers
{    
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository<Employee> _employeeRepository;

        // Constructor
        public EmployeeController(IEmployeeRepository<Employee> employeeRepository) 
        {
            _employeeRepository = employeeRepository;
        }

        // GET
        [HttpGet]
        public async Task<IActionResult> GetAllEmployees()
        {
            try
            {
                var employees = await _employeeRepository.GetAll();
                return Ok(employees);
            }
            catch (Exception)
            {
                return StatusCode(400, "BadRequest");
            }            
        }

        // GET: api/Employee/GUID
        [HttpGet]
        public async Task<IActionResult> GetEmployeeByID(Guid id)
        {
            try
            {
                var singleEmployee = await _employeeRepository.FindById(id);

                if (singleEmployee == null) { return StatusCode(404, "NotFound"); }
                else { return Ok(singleEmployee); }
            }
            catch (Exception)
            {
                return StatusCode(400, "BadRequest");
            }
        }

        // POST: api/Employee
        [HttpPost]
        public async Task<IActionResult> CreateEmployee([FromBody] Employee employee)
        {
            try
            {
                if (employee == null) { return BadRequest("Employee object is null."); }
                if (!ModelState.IsValid)  { return BadRequest("Invalid Employee object."); }

                _employeeRepository.Add(employee);
                var resourceUrl = Request.Path.ToString() + '/' + employee.EmployeeId;
                return Created(resourceUrl, employee);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error");
            }                        
        }
        // PUT: api/Employee/GUID
        [HttpPut]
        public async Task<IActionResult> UpdateEmployee(Guid id, [FromBody] Employee employee)
        {
            try
            {
                if (employee == null) { return BadRequest("Employee object is null."); }
                if (!ModelState.IsValid) { return BadRequest("Invalid Employee object."); }

                var employeeToUpdate = await _employeeRepository.FindById(id);
                
                if (employeeToUpdate == null) { return NotFound("The Employee record couldn't be found."); }

                _employeeRepository.Update(employeeToUpdate, employee);
                
                return Accepted();
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error");
            }
        }
        // DELETE: api/Employee/GUID
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployeeByID(Guid id)
        {
            try
            {
                var deletedEmployee = await _employeeRepository.FindById(id);
                
                if (deletedEmployee == null) { return StatusCode(404, "NotFound"); }

                _employeeRepository.Delete(deletedEmployee);

                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal Server Error");
            }
        }
    }
}
